clear;
% file downloaded from  https://drive.google.com/file/d/1ev1PCpxKYg47ohglfXdqzW-oROtwnllu/view?usp=sharing
file_name = 'z3_1350V_x20.dat';
file_ID = fopen(file_name, 'r'); %creating file for reading
file_list = dir(file_name); % list file properties

chop_memory = 50e6; %optimal data size for reading
count = zeros(1,256); %creating matrix for saving data

for k = 1:fix(file_list.bytes / chop_memory) + 1
    data = fread(file_ID, chop_memory,'uint8'); % read data with specified size from file
    for j = 1: size(data,1)      
        count(data(j)+1) = count(data(j)+1) + 1; %counting occurences of each value
    end
end  
fclose(file_ID); % closing file
values = (0:255); 

bar(values, count); % creating histogram

%setting title and labels for X and Y axis
title('Histogram'); 
xlabel('Values');
ylabel('Number of occurrences');

saveas(gcf, 'z3_1350V_x20_histogram.pdf'); % saving created graph as pdf file

noise_level  = NoiseLevel(count); % finding noise level

file_ID = fopen(file_name, 'r'); % creating file for reading
pulse_counter = 0; % creating counter

for k=1:fix(file_list.bytes / chop_memory) + 1
    data = fread(file_ID, chop_memory,'uint8'); %read data with specified size from file
    pulse_counter = pulse_counter + CountPulses(data - noise_level); %counting pulses
end
fclose(file_ID);% closing file

file_ID = fopen('z3_1350V_x20_peak_analysis_results.dat', 'w'); %creating new file for writing
fprintf(file_ID, 'Analyzed file: z3_1350V_x20.dat \nNoise level = %d \nCounted pulses = %d , uncertainty = %.2f \n', noise_level, pulse_counter, sqrt(pulse_counter)); %writing data to file
fclose(file_ID);% closing file