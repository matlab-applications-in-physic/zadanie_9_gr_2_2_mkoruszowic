function [ noise ] =  NoiseLevel( data )
% function for finding noise level
    [max_v, i]= max(data); % finding maximum of occurrences
    noise = i-1; % noise index
end