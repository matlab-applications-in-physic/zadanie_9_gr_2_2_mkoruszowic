function [ peak ] = CountPulses( data )
    % function that counts pulses
    % needed variables
    peak = 0;
    threshold = 4;
    comp = 0;
    resolution = 100;
    temp = 0; % previous value
    threshold_disrupiton = -2;
    for k = 1: fix(size(data,1) / resolution ) 
        j = (k-1) * resolution + 1;
        while j < k * resolution
            if data(j) < threshold_disrupiton
                break %breaking loop after finding value below treshold_disrupiton
            
            elseif data(j) > threshold && comp == 0
                % finding value above threshold
                if data(j) == temp
                    j = j + 1;
                else
                    comp = data(j); 
                end
            elseif data(j) > threshold && comp == data(j) && data(j) ~= temp
                % if founded value is equal to comp , count peak
                peak = peak + 1;
                comp = 0;
                break % after finding pulse end loop
            end                
            temp = data(j);
            j = j + 1;    
        end
    end
end